#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "rfdt.h"

unsigned char *ptr;

static int32_t memread(void *user, void *dest, uint32_t src_offset, int32_t count){
	memcpy(dest, &ptr[src_offset], count);
	return(count);
}

static int32_t memwrite(void *user, uint32_t dest_offset, const void *src, int32_t count){
	memcpy(&ptr[dest_offset], src, count);
	return(count);
}

int main(int argc, char *argv[]){
	uint32_t e, error_value;
	rfdt_token_t token;
	struct stat s;
	rfdt_t fdt;
	int fd;

	fd = open(argv[1], O_RDWR);
	fstat(fd, &s);
	ptr = mmap(NULL, s.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	e = rfdt_init(&fdt, NULL, memread, memwrite, &error_value);
	if(e < 0){
		if(e == RFDT_ERROR_READ){
			printf("FDT: Read error\n");
		}else if(e == RFDT_ERROR_WRONG_MAGIC){
			printf("FDT: Wrong magic: 0x%08X\n", (unsigned int)error_value);
		}else if(e == RFDT_ERROR_TOTAL_SIZE_TOO_SMALL){
			printf("FDT: Total size too small\n");
		}else if(e == RFDT_ERROR_UNSUPPORTED_COMPATIBLE_VERSION){
			printf("FDT: Unsupported compatible version: %u\n", (unsigned int)error_value);
		}else if(e == RFDT_ERROR_VERSION){
			printf("FDT: Version error: %u\n", (unsigned int)error_value);
		}else if(e == RFDT_ERROR_MEMORY_RESERVATION_BLOCK_OFFSET){
			printf("FDT: Invalid memory reservation block offset\n");
		}else if(e == RFDT_ERROR_STRUCTURE_BLOCK_OFFSET){
			printf("FDT: Invalid structure block offset\n");
		}else if(e == RFDT_ERROR_STRUCTURE_BLOCK_SIZE){
			printf("FDT: Invalid structure block size\n");
		}else if(e == RFDT_ERROR_STRINGS_BLOCK_OFFSET){
			printf("FDT: Invalid strings block offset\n");
		}else if(e == RFDT_ERROR_STRINGS_BLOCK_SIZE){
			printf("FDT: Invalid strings block size\n");
		}
		return(-1);
	}

	rfdt_token_root(&fdt, &token);
	while(1){
		if(rfdt_token_next(&fdt, &token, &token) < 0)break;
		printf("%s\n", token.name);
	}
	return(0);
}



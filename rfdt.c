#include "rfdt.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

int rfdt_init(rfdt_t *fdt, void *user, rfdt_read_t read, rfdt_write_t write, uint32_t *error_value){
	uint8_t buffer[40];

	fdt->user = user;
	fdt->read = read;
	fdt->write = write;

	if(fdt->read(fdt->user, buffer, 0, 40) < 40)return(RFDT_ERROR_READ);
	*error_value = rfdt_uint32(&buffer[RFDT_HEADER_MAGIC]);
	if(*error_value != RFDT_MAGIC)return(RFDT_ERROR_WRONG_MAGIC);
	fdt->totalsize = rfdt_uint32(&buffer[RFDT_HEADER_TOTALSIZE]);
	if(fdt->totalsize < (RFDT_MIN_HEADER + RFDT_MIN_MEM_RSVMAP + RFDT_MIN_DT_STRUCT + RFDT_MIN_DT_STRINGS))return(RFDT_ERROR_TOTAL_SIZE_TOO_SMALL);
	*error_value = rfdt_uint32(&buffer[RFDT_HEADER_LAST_COMP_VERSION]);
	if((*error_value < 16) || (*error_value > 17))return(RFDT_ERROR_UNSUPPORTED_COMPATIBLE_VERSION);
	fdt->version = rfdt_uint32(&buffer[RFDT_HEADER_VERSION]);
	if(fdt->version < rfdt_uint32(&buffer[RFDT_HEADER_LAST_COMP_VERSION])){
		*error_value = fdt->version;
		return(RFDT_ERROR_VERSION);
	}
	fdt->off_mem_rsvmap = rfdt_uint32(&buffer[RFDT_HEADER_OFF_MEM_RSVMAP]);
	if((fdt->off_mem_rsvmap < RFDT_MIN_HEADER) || (fdt->off_mem_rsvmap > (fdt->totalsize - RFDT_MIN_MEM_RSVMAP)))return(RFDT_ERROR_MEMORY_RESERVATION_BLOCK_OFFSET);
	fdt->off_dt_struct = rfdt_uint32(&buffer[RFDT_HEADER_OFF_DT_STRUCT]);
	if((fdt->off_dt_struct < (fdt->off_mem_rsvmap + RFDT_MIN_MEM_RSVMAP)) || (fdt->off_dt_struct > (fdt->totalsize - RFDT_MIN_DT_STRUCT)))return(RFDT_ERROR_STRUCTURE_BLOCK_OFFSET);
	fdt->size_dt_struct = rfdt_uint32(&buffer[RFDT_HEADER_SIZE_DT_STRUCT]);
	if((fdt->size_dt_struct < RFDT_MIN_DT_STRUCT) || ((fdt->off_dt_struct + fdt->size_dt_struct) > fdt->totalsize))return(RFDT_ERROR_STRUCTURE_BLOCK_SIZE);
	fdt->off_dt_strings = rfdt_uint32(&buffer[RFDT_HEADER_OFF_DT_STRINGS]);
	if((fdt->off_dt_strings < (fdt->off_dt_struct + fdt->size_dt_struct)) || (fdt->off_dt_strings > (fdt->totalsize - RFDT_MIN_DT_STRINGS)))return(RFDT_ERROR_STRINGS_BLOCK_OFFSET);
	fdt->size_dt_strings = rfdt_uint32(&buffer[RFDT_HEADER_SIZE_DT_STRINGS]);
	if((fdt->size_dt_strings < RFDT_MIN_DT_STRINGS) || ((fdt->off_dt_strings + fdt->size_dt_strings) > fdt->totalsize))return(RFDT_ERROR_STRINGS_BLOCK_SIZE);
	return(1);
}

int rfdt_token_root(rfdt_t *fdt, rfdt_token_t *token){
	uint8_t buffer[256];
	uint32_t i;

	if(fdt->read(fdt->user, buffer, fdt->off_dt_struct, 12) < 12)goto error;
	token->type = rfdt_uint32(&buffer[0]);
	if(token->type != RFDT_BEGIN_NODE)goto error;
	for(i = 0; ; i++){
		if(!(i & (256 - 1))){
			if(fdt->read(fdt->user, buffer, fdt->off_dt_struct + 4 + i, 256) < 256)goto error;
			if(i == 0){
				memcpy(token->name, &buffer[0], 256);
				token->name[255] = 0;
			}
		}
		if(buffer[i] == 0){
			i++;
			if(i % 4)i += (4 - (i % 4));
			token->next = 4 + i;
			break;
		}
	}
	return(1);
error:
	token->type = RFDT_ERROR;
	token->next = RFDT_ERROR;
	token->name[0] = 0;
	return(-1);
}

int rfdt_token_next(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token){
	uint8_t buffer[256];
	rfdt_token_t r;
	uint32_t i;

	if(root == NULL){
		if(rfdt_token_root(fdt, &r) < 0)goto error;
		root = &r;
	}

	if(root->next == RFDT_ERROR)goto error;
	if(fdt->read(fdt->user, buffer, fdt->off_dt_struct + root->next, 12) < 12)goto error;
	token->type = rfdt_uint32(&buffer[0]);
	if(token->type == RFDT_BEGIN_NODE){
		for(i = 0; ; i++){
			if(!(i & (256 - 1))){
				if(fdt->read(fdt->user, buffer, fdt->off_dt_struct + root->next + 4 + i, 256) < 256)goto error;
				if(i == 0){
					memcpy(token->name, &buffer[0], 256);
					token->name[255] = 0;
				}
			}
			if(buffer[i] == 0){
				i++;
				if(i % 4)i += (4 - (i % 4));
				token->next = root->next + 4 + i;
				return(1);
			}
		}
	}else if(token->type == RFDT_END_NODE){
		token->next = root->next + 4;
		token->name[0] = 0;
		return(1);
	}else if(token->type == RFDT_PROP){
		token->data_offset = root->next + 4 + 8;
		token->data_len = rfdt_uint32(&buffer[4]);
		if(token->data_len % 4)root->next += (4 - (token->data_len % 4));
		token->next = root->next + 4 + 8 + token->data_len;
		i = rfdt_uint32(&buffer[8]);
		if(fdt->read(fdt->user, token->name, fdt->off_dt_strings + i, 256) < 256)goto error;
		token->name[255] = 0;
		return(1);
	}else if(token->type == RFDT_NOP){
		token->next = root->next + 4;
		token->name[0] = 0;
		return(1);
	}else if(token->type == RFDT_END){
		token->next = RFDT_ERROR;
		token->name[0] = 0;
		return(1);
	}
error:
	token->type = RFDT_ERROR;
	token->next = RFDT_ERROR;
	token->name[0] = 0;
	return(-1);
}

int rfdt_token_path(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token, const char *path){
	int level, wanted, s, l;
	rfdt_token_t r;

	if(root == NULL){
		if(rfdt_token_root(fdt, &r) < 0)goto error;
		root = &r;
	}

	level = 0;
	wanted = 0;
	for(s = 0; path[s] == '/'; s++);
	*token = *root;
	while(1){
		for(l = s; path[l] && (path[l] != '/'); l++);
		if(rfdt_token_next(fdt, token, token) < 0)break;
		if(token->type == RFDT_BEGIN_NODE){
			if((level == wanted) && ((l - s) == strlen(token->name)) && !strncasecmp(token->name, &path[s], l - s)){
				if(path[l] == 0)return(1);
				wanted++;
				s = l + 1;
			}
			level++;
		}else if(token->type == RFDT_END_NODE){
			if((level <= 0) || (level <= wanted))break;
			level--;
		}else if(token->type == RFDT_PROP){
			if((level == wanted) && (path[l] == 0) && ((l - s) == strlen(token->name)) && !strncasecmp(token->name, &path[s], l - s))return(1);
		}
	}
error:
	token->type = RFDT_ERROR;
	token->next = RFDT_ERROR;
	token->name[0] = 0;
	return(-1);
}

int rfdt_token_path_va(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token, ...){
	int level, wanted, ret;
	char *name, *next;
	rfdt_token_t r;
	va_list ap;

	ret = -1;
	if(root == NULL){
		if(rfdt_token_root(fdt, &r) < 0)goto out;
		root = &r;
	}

	level = 0;
	wanted = 0;
	*token = *root;
	va_start(ap, token);
	name = va_arg(ap, char *);
	next = va_arg(ap, char *);
	while(1){
		if(rfdt_token_next(fdt, token, token) < 0)break;
		if(token->type == RFDT_BEGIN_NODE){
			if((level == wanted) && !strcasecmp(token->name, name)){
				if(next == NULL){
					ret = 1;
					break;
				}else{
					wanted++;
					name = next;
					next = va_arg(ap, char *);
				}
			}
			level++;
		}else if(token->type == RFDT_END_NODE){
			if((level <= 0) || (level <= wanted))break;
			level--;
		}else if(token->type == RFDT_PROP){
			if((level == wanted) && (next == NULL) && !strcasecmp(token->name, name)){
				ret = 1;
				break;
			}
		}
	}
	va_end(ap);
out:
	if(ret < 0){
		token->type = RFDT_ERROR;
		token->next = RFDT_ERROR;
		token->name[0] = 0;
	}
	return(ret);
}

int rfdt_token_child(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token, int index){
	int level, i;
	rfdt_token_t r;

	if(root == NULL){
		if(rfdt_token_root(fdt, &r) < 0)goto error;
		root = &r;
	}

	level = 0;
	i = 0;
	*token = *root;
	while(1){
		if(rfdt_token_next(fdt, token, token) < 0)break;
		if(token->type == RFDT_BEGIN_NODE){
			if(level == 0){
				if(i == index)return(1);
				i++;
			}
			level++;
		}else if(token->type == RFDT_END_NODE){
			if(level <= 0)break;
			level--;
		}else if(token->type == RFDT_PROP){
			if(level == 0){
				if(i == index)return(1);
				i++;
			}
		}
	}
error:
	token->type = RFDT_ERROR;
	token->next = RFDT_ERROR;
	token->name[0] = 0;
	return(-1);
}

int32_t rfdt_token_data(rfdt_t *fdt, rfdt_token_t *token, void *dest, uint32_t offset, int32_t count){
	int32_t l;
	if(count < 1)return(count);
	if(offset > token->data_len)return(0);
	l = token->data_len - offset;
	if(count > l)count = l;
	return(fdt->read(fdt->user, dest, fdt->off_dt_struct + token->data_offset + offset, count));
}

int32_t rfdt_token_string(rfdt_t *fdt, rfdt_token_t *token, char *dest, int32_t count){
	if(count < 1)return(count);
	count--;
	if(count > token->data_len)count = token->data_len;
	dest[count] = 0;
	return(fdt->read(fdt->user, dest, fdt->off_dt_struct + token->data_offset, count));
}

uint32_t rfdt_token_uint32(rfdt_t *fdt, rfdt_token_t *token, int index){
	uint8_t buffer[4];
	buffer[0] = buffer[1] = buffer[2] = buffer[3] = 0;
	rfdt_token_data(fdt, token, buffer, (index * 4), 4);
	return(rfdt_uint32(buffer));
}

int32_t rfdt_token_data_set(rfdt_t *fdt, rfdt_token_t *token, uint32_t offset, const void *src, int32_t count){
	int32_t l;
	if(fdt->write == NULL)return(-1);
	if(count < 1)return(count);
	if(offset > token->data_len)return(0);
	l = token->data_len - offset;
	if(count > l)count = l;
	return(fdt->write(fdt->user, fdt->off_dt_struct + token->data_offset + offset, src, count));
}

int32_t rfdt_token_uint32_set(rfdt_t *fdt, rfdt_token_t *token, int index, uint32_t value){
	uint8_t buffer[4];
	if(fdt->write == NULL)return(-1);
	buffer[0] = (value >> 24) & 0xFF;
	buffer[1] = (value >> 16) & 0xFF;
	buffer[2] = (value >> 8) & 0xFF;
	buffer[3] = (value >> 0) & 0xFF;
	return(rfdt_token_data_set(fdt, token, (index * 4), buffer, 4));
}

uint32_t rfdt_token_offset(rfdt_t *fdt, rfdt_token_t *token){
	return(fdt->off_dt_struct + token->data_offset);
}


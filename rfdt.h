#ifndef _RFDT_H_
#define _RFDT_H_

#include <stdint.h>
#include <stdarg.h>

#define RFDT_MAGIC                    0xD00DFEED
#define RFDT_ERROR                    0xFFFFFFFF
#define rfdt_uint32(p)                (((uint32_t)((uint8_t *)(p))[0] << 24) | ((uint32_t)((uint8_t *)(p))[1] << 16) | ((uint32_t)((uint8_t *)(p))[2] << 8) | ((uint32_t)((uint8_t *)(p))[3] << 0))

#define RFDT_MIN_HEADER               40
#define RFDT_MIN_MEM_RSVMAP           16 // uint64 + uint64
#define RFDT_MIN_DT_STRUCT            16 // RFDT_BEGIN_NODE + name + RFDT_END_NODE + RFDT_END
#define RFDT_MIN_DT_STRINGS           0  // (0x00 + padding ???)

#define RFDT_HEADER_MAGIC             0
#define RFDT_HEADER_TOTALSIZE         4
#define RFDT_HEADER_OFF_DT_STRUCT     8
#define RFDT_HEADER_OFF_DT_STRINGS    12
#define RFDT_HEADER_OFF_MEM_RSVMAP    16
#define RFDT_HEADER_VERSION           20
#define RFDT_HEADER_LAST_COMP_VERSION 24
#define RFDT_HEADER_BOOT_CPUID_PHYS   28
#define RFDT_HEADER_SIZE_DT_STRINGS   32
#define RFDT_HEADER_SIZE_DT_STRUCT    36

#define RFDT_BEGIN_NODE               0x00000001
#define RFDT_END_NODE                 0x00000002
#define RFDT_PROP                     0x00000003
#define RFDT_NOP                      0x00000004
#define RFDT_END                      0x00000009

#define RFDT_ERROR_READ                            (-1)
#define RFDT_ERROR_WRONG_MAGIC                     (-2)
#define RFDT_ERROR_TOTAL_SIZE_TOO_SMALL            (-3)
#define RFDT_ERROR_UNSUPPORTED_COMPATIBLE_VERSION  (-4)
#define RFDT_ERROR_VERSION                         (-5)
#define RFDT_ERROR_MEMORY_RESERVATION_BLOCK_OFFSET (-6)
#define RFDT_ERROR_STRUCTURE_BLOCK_OFFSET          (-7)
#define RFDT_ERROR_STRUCTURE_BLOCK_SIZE            (-8)
#define RFDT_ERROR_STRINGS_BLOCK_OFFSET            (-9)
#define RFDT_ERROR_STRINGS_BLOCK_SIZE              (-10)

typedef int32_t (*rfdt_read_t)(void *user, void *dest, uint32_t offset, int32_t count);
typedef int32_t (*rfdt_write_t)(void *user, uint32_t dest_offset, const void *src, int32_t count);

struct rfdt_struct{
	rfdt_read_t read;
	rfdt_write_t write;
	void *user;
	uint32_t version;
	uint32_t totalsize;
	uint32_t off_dt_struct;
	uint32_t off_dt_strings;
	uint32_t off_mem_rsvmap;
	uint32_t size_dt_strings;
	uint32_t size_dt_struct;
};

typedef struct rfdt_struct rfdt_t;

struct rfdt_token_struct{
	uint32_t type;
	char name[256];
	uint32_t data_offset;
	uint32_t data_len;
	uint32_t next;
};

typedef struct rfdt_token_struct rfdt_token_t;

int rfdt_init(rfdt_t *fdt, void *user, rfdt_read_t read, rfdt_write_t write, uint32_t *error_value);

int rfdt_token_root(rfdt_t *fdt, rfdt_token_t *token);
int rfdt_token_next(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token);
int rfdt_token_path(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token, const char *path);
int rfdt_token_path_va(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token, ...);
int rfdt_token_child(rfdt_t *fdt, rfdt_token_t *root, rfdt_token_t *token, int index);

int32_t rfdt_token_data(rfdt_t *fdt, rfdt_token_t *token, void *dest, uint32_t offset, int32_t count);
int32_t rfdt_token_string(rfdt_t *fdt, rfdt_token_t *token, char *dest, int32_t count);
uint32_t rfdt_token_uint32(rfdt_t *fdt, rfdt_token_t *token, int index);

int32_t rfdt_token_data_set(rfdt_t *fdt, rfdt_token_t *token, uint32_t offset, const void *src, int32_t count);
int32_t rfdt_token_uint32_set(rfdt_t *fdt, rfdt_token_t *token, int index, uint32_t value);

uint32_t rfdt_token_offset(rfdt_t *fdt, rfdt_token_t *token);

#endif

